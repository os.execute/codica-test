# frozen_string_literal: true

class PatientsController < ApplicationController
  def home
    @patient = current_user
    authorize! :read, @patient

    load_relations
    render 'show'
  end

  def show
    @patient = Patient.find(params[:id])
    authorize! :read, @patient

    load_relations
  end

  private

  def load_relations
    @recommendations = @patient.recommendations.includes(appointment: [{ doctor: :doctor_category }, :patient])
    @appointments = @patient.appointments.open.includes({ doctor: :doctor_category }, :patient)
  end
end
