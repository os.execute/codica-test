# frozen_string_literal: true

class DeviseCreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      ## Database authenticatable
      t.string :phone_number,       null: false
      t.string :encrypted_password, null: false, default: ''

      ## Rememberable
      t.datetime :remember_created_at

      # STI
      t.string :type, null: false

      # Timestampts
      t.timestamps null: false
    end

    add_index :users, :phone_number, unique: true
  end
end
