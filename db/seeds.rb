# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

patient_phone = '+380939960128'
unless User.find_by(phone_number: patient_phone)
  Patient.create(
    phone_number: patient_phone,
    full_name: 'Nicolas Supercage',
    password: 'patient'
  )
end

doctor_phone = '+380939960127'
unless User.find_by(phone_number: doctor_phone)
  Doctor.create(
    phone_number: doctor_phone,
    full_name: 'Artem Yurchenko',
    password: 'doctor'
  )
end

admin_phone = '+380939960129'
unless User.find_by(phone_number: admin_phone)
  AdminUser.create(
    phone_number: admin_phone,
    full_name: 'Adminis Trator',
    password: 'admin'
  )
end

class SampleSeeder
  include FactoryBot::Syntax::Methods

  def call
    return unless Rails.env.development?

    (1 * multiplier).times { create_doctor_category }
    (8 * multiplier).times { create_doctor }
    (8 * multiplier).times { create_patient }
    (64 * multiplier).times { create_appointment }
    (32 * multiplier).times { create_recommendation }
  end

  private

  def multiplier
    32
  end

  def create_doctor
    doctor_category = DoctorCategory.all.sample
    doctor_category = nil if Faker::Boolean.boolean(true_ratio: 0.3)

    create(:doctor, doctor_category:)
  end

  def create_doctor_category
    create(:doctor_category)
  rescue ActiveRecord::RecordInvalid
    nil
  end

  def create_patient
    create(:patient)
  end

  def create_appointment
    doctor = Doctor.all.sample
    patient = Patient.all.sample

    return unless doctor && patient

    create(:appointment, doctor:, patient:)
  end

  def create_recommendation
    appointment = Appointment.open.sample
    return unless appointment

    create(:recommendation, appointment:)
  end
end

SampleSeeder.new.call
