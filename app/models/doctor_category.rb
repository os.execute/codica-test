# frozen_string_literal: true

# == Schema Information
#
# Table name: doctor_categories
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class DoctorCategory < ApplicationRecord
  has_many :doctors

  validates :name, uniqueness: true, presence: true
end
