# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Appointments', type: :request do
  # describe "GET /appointments" do
  #   it "works! (now write some real specs)" do
  #     get appointments_path
  #     expect(response).to have_http_status(200)
  #   end
  # end
  describe 'POST /doctor/:doctor_id/appointments/create' do
    let(:doctor) { create(:doctor) }

    context 'not logged in' do
      it 'redirects to login page' do
        post create_appointment_path(doctor)
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'logged in as Doctor' do
      before { login_as create(:doctor) }

      it 'does not allow doctor to create new appointments, raises `AccessDenied` error' do
        expect do
          post create_appointment_path(doctor)
        end.to raise_error(CanCan::AccessDenied)
      end
    end

    context 'logged in as Patient' do
      let(:patient) { create(:patient) }
      let(:params) { { params: { appointment: { patient_id: patient.id } } } }
      before { login_as patient }

      context 'doctor exists and has no appointments' do
        it 'creates new appointment' do
          expect do
            post create_appointment_path(doctor, params)
          end.to change { Appointment.count }.by(1)

          expect(response).to redirect_to(patient_home_path)
        end
      end

      context 'doctor does not exist' do
        it 'raises not_found error' do
          expect do
            post create_appointment_path(params.merge(doctor_id: doctor.id + 100))
          end.to raise_error(ActiveRecord::RecordNotFound)
        end
      end

      context 'doctro exists but already has 10 open appointments' do
        before do
          create_list(:appointment, 10, doctor:)
        end

        it 'fails to create new appointment, raises validation `RecordInvalid` error' do
          expect do
            post create_appointment_path(doctor, params)
          end.to raise_error(ActiveRecord::RecordInvalid)
        end
      end
    end
  end
end
