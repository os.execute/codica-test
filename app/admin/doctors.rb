# frozen_string_literal: true

ActiveAdmin.register Doctor do
  menu priority: 2
  menu parent: 'User'

  permit_params :phone_number, :encrypted_password, :full_name, :doctor_category

  includes :appointments, :recommendations

  filter :full_name
  filter :phone_number
  filter :doctor_category

  index do
    id_column
    column :phone_number
    column :full_name
    column :doctor_category
    actions
  end

  show do
    para doctor.full_name
    para doctor.phone_number

    table_for doctor.appointments do
      column :patient
      column :description
      column :open?
      column :recommendation
    end

    table_for doctor.recommendations do
      column :patient
      column :appointment
      column :content
    end
  end
end
