# frozen_string_literal: true

ActiveAdmin.register Patient do
  menu priority: 2
  menu parent: 'User'

  permit_params :phone_number, :encrypted_password, :full_name

  includes :appointments, :recommendations

  filter :full_name
  filter :phone_number

  index do
    id_column
    column :phone_number
    column :full_name
    actions
  end

  show do
    para patient.full_name
    para patient.phone_number

    table_for patient.appointments do
      column :doctor
      column :description
      column :open?
      column :recommendation
    end

    table_for patient.recommendations do
      column :doctor
      column :appointment
      column :content
    end
  end
end
