# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                 :bigint           not null, primary key
#  encrypted_password :string           default(""), not null
#  full_name          :string
#  phone_number       :string           not null
#  type               :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  doctor_category_id :integer
#
# Indexes
#
#  index_users_on_phone_number  (phone_number) UNIQUE
#
class User < ApplicationRecord
  devise :database_authenticatable, :registerable

  validates :phone_number, presence: true, format: /\A\+\d{10,16}\z/

  def patient?
    type == 'Patient'
  end

  def doctor?
    type == 'Doctor'
  end

  def admin?
    type == 'AdminUser'
  end
end
