# frozen_string_literal: true

# == Schema Information
#
# Table name: appointments
#
#  id          :bigint           not null, primary key
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  doctor_id   :integer          not null
#  patient_id  :integer          not null
#
FactoryBot.define do
  factory :appointment do
    description { "I feel too #{Faker::Emotion.adjective}" }

    doctor
    patient

    trait :closed do
      recommendation
    end
  end
end
