# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                 :bigint           not null, primary key
#  encrypted_password :string           default(""), not null
#  full_name          :string
#  phone_number       :string           not null
#  type               :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  doctor_category_id :integer
#
# Indexes
#
#  index_users_on_phone_number  (phone_number) UNIQUE
#
require 'rails_helper'
require 'models/shared_examples/users_samples'

RSpec.describe Patient, type: :model do
  it 'is possible to create' do
    expect { create(:patient) }.not_to raise_error
  end

  describe 'validations' do
    # TODO: Move this validation spec to shared examples
    include_examples 'validates the phone_number'
  end

  describe 'relations' do
    it { is_expected.to have_many(:appointments) }
    it { is_expected.to have_many(:doctors).through(:appointments) }
    it { is_expected.to have_many(:recommendations).through(:appointments) }
  end
end
