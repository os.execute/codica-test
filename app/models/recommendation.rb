# frozen_string_literal: true

# == Schema Information
#
# Table name: recommendations
#
#  id             :bigint           not null, primary key
#  content        :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  appointment_id :bigint           not null
#
# Indexes
#
#  index_recommendations_on_appointment_id  (appointment_id)
#
# Foreign Keys
#
#  fk_rails_...  (appointment_id => appointments.id)
#
class Recommendation < ApplicationRecord
  belongs_to :appointment
  has_one :patient, through: :appointment
  has_one :doctor, through: :appointment

  validates :content, presence: true
end
