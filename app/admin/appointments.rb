# frozen_string_literal: true

ActiveAdmin.register Appointment do
  menu priority: 7

  permit_params :doctor_id, :patient_id, :description

  filter :doctor
  filter :patient
  filter :description

  scope :open
  scope :closed
end
