# frozen_string_literal: true

# == Schema Information
#
# Table name: recommendations
#
#  id             :bigint           not null, primary key
#  content        :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  appointment_id :bigint           not null
#
# Indexes
#
#  index_recommendations_on_appointment_id  (appointment_id)
#
# Foreign Keys
#
#  fk_rails_...  (appointment_id => appointments.id)
#
FactoryBot.define do
  factory :recommendation do
    content { "Take #{Faker::Color.color_name} pills #{Faker::Number.non_zero_digit} times." }

    appointment
  end
end
