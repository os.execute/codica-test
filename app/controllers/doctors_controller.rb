# frozen_string_literal: true

class DoctorsController < ApplicationController
  def home
    @doctor = current_user
    authorize! :show, @doctor

    load_relations
    render 'show'
  end

  def show
    @doctor = Doctor.find(params[:id])
    authorize! :show, @doctor

    load_relations
  end

  def index
    authorize! :index, Doctor

    @doctor_categories = DoctorCategory.all
    doctors_query = params[:doctor_category] ? DoctorCategory.find(params[:doctor_category]).doctors : Doctor.all

    @doctors = doctors_query.includes(:doctor_category)
  end

  private

  def load_relations
    @recommendations = @doctor.recommendations.includes(appointment: [{ doctor: :doctor_category }, :patient])
    @appointments = @doctor.appointments.open.includes({ doctor: :doctor_category }, :patient)
  end
end
