# frozen_string_literal: true

# == Schema Information
#
# Table name: doctor_categories
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

RSpec.describe DoctorCategory, type: :model do
  it 'is possible to create' do
    expect { create(:doctor_category) }.not_to raise_error
  end

  describe 'relations' do
    it { is_expected.to have_many(:doctors) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name) }
  end
end
