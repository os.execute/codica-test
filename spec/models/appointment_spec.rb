# frozen_string_literal: true

# == Schema Information
#
# Table name: appointments
#
#  id          :bigint           not null, primary key
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  doctor_id   :integer          not null
#  patient_id  :integer          not null
#
require 'rails_helper'

RSpec.describe Appointment, type: :model do
  it 'is possible to create' do
    expect { create(:appointment) }.not_to raise_error
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:patient) }
    it { is_expected.to validate_presence_of(:doctor) }

    describe '#doctor_appointments_limit validation' do
      let(:doctor) { create(:doctor) }

      context 'doctor has no appointments at all' do
        it 'allows to create an appointment' do
          expect do
            create(:appointment, doctor:)
          end.not_to raise_error
        end

        it 'allows to create 10 appointments' do
          expect do
            10.times { create(:appointment, doctor:) }
          end.not_to raise_error
        end

        it 'fails to create 11 appointments' do
          expect do
            11.times { create(:appointment, doctor:) }
          end.to raise_error(ActiveRecord::RecordInvalid)
        end
      end

      context 'doctor has 10 open appointments' do
        before do
          10.times { create(:appointment, doctor:) }
        end

        it 'does not allow to create one more appointment' do
          expect do
            create(:appointment, doctor:)
          end.to raise_error(ActiveRecord::RecordInvalid)
        end
      end

      context 'doctor has 10 closed appointments' do
        before do
          10.times { create(:appointment, :closed, doctor:) }
        end

        it 'allows to create one more appointment' do
          expect do
            create(:appointment, doctor:)
          end.not_to raise_error
        end

        it 'does not allow to create 11 more appointments' do
          expect do
            11.times { create(:appointment, doctor:) }
          end.to raise_error(ActiveRecord::RecordInvalid)
        end
      end
    end
  end

  describe 'relations' do
    it { is_expected.to belong_to(:doctor).required(true) }
    it { is_expected.to belong_to(:patient).required(true) }
    it { is_expected.to have_one(:recommendation).required(false) }
  end

  describe 'open status based on presence of recommendation' do
    5.times do |n|
      let!(:"appointment_#{n}") { create(:appointment, description: "Appointment #{n}") }
    end

    before do
      create(:recommendation, appointment: appointment_0)
      create(:recommendation, appointment: appointment_1)
    end

    describe 'scopes' do
      let(:closed_appointments) { [appointment_0, appointment_1] }
      let(:open_appointments) { [appointment_2, appointment_3, appointment_4] }

      describe 'closed' do
        it 'selects appointments with recommendations' do
          expect(Appointment.closed).to include(*closed_appointments)
          expect(Appointment.closed).not_to include(*open_appointments)
        end
      end

      describe 'open' do
        it 'selects appointments without recommendations' do
          expect(Appointment.open).to include(*open_appointments)
          expect(Appointment.open).not_to include(*closed_appointments)
        end
      end
    end

    describe '#open?' do
      it 'returns true for appointment without recommendation' do
        expect(appointment_2.open?).to eq(true)
      end

      it 'returns false for appointment with recommendation' do
        expect(appointment_0.open?).to eq(false)
      end
    end

    describe '#closed?' do
      it 'returns true for appointment with recommendation' do
        expect(appointment_1.closed?).to eq(true)
      end

      it 'returns false for appointment without recommendation' do
        expect(appointment_3.closed?).to eq(false)
      end
    end
  end
end
