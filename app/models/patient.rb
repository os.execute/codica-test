# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                 :bigint           not null, primary key
#  encrypted_password :string           default(""), not null
#  full_name          :string
#  phone_number       :string           not null
#  type               :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  doctor_category_id :integer
#
# Indexes
#
#  index_users_on_phone_number  (phone_number) UNIQUE
#
class Patient < User
  has_many :appointments, dependent: :destroy
  has_many :doctors, through: :appointments
  has_many :recommendations, through: :appointments, dependent: :destroy

  validates :full_name, presence: true
end
