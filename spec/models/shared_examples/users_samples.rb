# frozen_string_literal: true

RSpec.shared_examples 'validates the phone_number', shared_context: :metadata do
  describe 'phone_number' do
    let(:valid_numbers) do
      %w[+380939960127 +380954523954 +111223334455]
    end

    let(:invalid_numbers) do
      %w[+380939 +38095452395443467 helloworld 53fw32fg p]
    end

    it { is_expected.to allow_values(*valid_numbers).for(:phone_number) }
    it { is_expected.not_to allow_values(*invalid_numbers).for(:phone_number) }
  end
end
