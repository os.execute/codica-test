# frozen_string_literal: true

# == Schema Information
#
# Table name: recommendations
#
#  id             :bigint           not null, primary key
#  content        :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  appointment_id :bigint           not null
#
# Indexes
#
#  index_recommendations_on_appointment_id  (appointment_id)
#
# Foreign Keys
#
#  fk_rails_...  (appointment_id => appointments.id)
#
require 'rails_helper'

RSpec.describe Recommendation, type: :model do
  it 'is possible to create' do
    expect { create(:recommendation) }.not_to raise_error
  end

  describe 'relations' do
    it { is_expected.to belong_to(:appointment).required(true) }
    it { is_expected.to have_one(:doctor).through(:appointment) }
    it { is_expected.to have_one(:patient).through(:appointment) }
  end

  describe 'validation' do
    it { is_expected.to validate_presence_of(:content) }
  end
end
