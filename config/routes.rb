# frozen_string_literal: true

Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Login/Register
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  # Root
  root to: 'home#index'

  # Patient pages
  get 'patient/home', to: 'patients#home'
  get 'patient/:id', to: 'patients#show'

  # Doctors pages
  get 'doctor/home', to: 'doctors#home'
  get 'doctor/:id', to: 'doctors#show'

  get 'doctors', to: 'doctors#index', as: :doctor_search

  # Appointments
  post 'appointment/:id/close', to: 'appointments#close', as: :close_appointment

  get 'doctor/:doctor_id/appointments/new', to: 'appointments#new', as: :new_appointment
  post 'doctor/:doctor_id/appointments/create', to: 'appointments#create', as: :create_appointment

  # Admin
  ActiveAdmin.routes(self)
end
