# frozen_string_literal: true

# == Schema Information
#
# Table name: appointments
#
#  id          :bigint           not null, primary key
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  doctor_id   :integer          not null
#  patient_id  :integer          not null
#
class Appointment < ApplicationRecord
  belongs_to :patient, foreign_key: :patient_id
  belongs_to :doctor, foreign_key: :doctor_id

  has_one :recommendation, dependent: :destroy

  validates :doctor, presence: true
  validates :patient, presence: true
  validates_associated :doctor

  after_validation :doctor_appointments_limit

  scope :closed, -> { joins(:recommendation) }
  scope :open, -> { where.not(id: closed) }

  def closed?
    recommendation.present?
  end

  def open?
    !closed?
  end

  def to_s
    description
  end

  private

  def doctor_appointments_limit
    return unless doctor

    errors.add(:doctor, 'Too many appointments') if doctor.appointments_full?
  end
end
