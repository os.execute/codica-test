# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the user here.
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/blob/develop/docs/define_check_abilities.md
    setup_patient_rights(user) if user.patient?

    setup_doctor_rights(user) if user.doctor?

    return unless user.admin?

    can :manage, :all
  end

  private

  def setup_patient_rights(user)
    can :read, Patient, itself: user
    can :read_number, Patient, itself: user
    can :index, Doctor
    can :create, Appointment, patient: user
    can :read_number, Doctor, recommendations: { patient: { itself: user } }
  end

  def setup_doctor_rights(user)
    can :read, Doctor, itself: user
    can :read_number, Doctor, itself: user
    can :read, Patient
    can :read_number, Patient
    can :close, Appointment, doctor: user
  end
end
