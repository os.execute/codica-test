# frozen_string_literal: true

class CreateDoctorCategories < ActiveRecord::Migration[7.0]
  def change
    create_table :doctor_categories do |t|
      t.string :name

      t.timestamps
    end

    add_column :users, :doctor_category_id, :integer, foreign_key: true
  end
end
