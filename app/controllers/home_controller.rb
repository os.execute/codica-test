# frozen_string_literal: true

# Controller to redirect users to different home pages
class HomeController < ApplicationController
  # GET /
  def index
    case current_user
    when Patient
      redirect_to controller: 'patients', action: 'home'
    when Doctor
      redirect_to controller: 'doctors', action: 'home'
    else
      redirect_to :admin_root
    end
  end
end
