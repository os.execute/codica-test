# frozen_string_literal: true

class CreateAppointments < ActiveRecord::Migration[7.0]
  def change
    create_table :appointments do |t|
      t.datetime :date
      t.integer :patient_id, null: false
      t.integer :doctor_id, null: false
      t.text :description

      t.timestamps
    end
  end
end
