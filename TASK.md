# `Codica` test task (Ruby on Rails)

## Entities:

**User**
- phone_number (uniqueness, validness)
- full_name

-> **Doctor**
 - belongs_to `doctor_category`
 - has_many `appointments` (open < 10)
 - has_many `patients` through `appointments`
 - has_many `recommendations` through `appointments`

-> **Patient**
  - has_many `appointments`
  - has_many `doctors` through `appointments`
  - has_many `recommendations` through `appointments`

-> **Admin**

**DoctorCategory**
- name (uniq, validness)
- has_many `doctors`

**Appointment**
- description
- status open/closed
- belongs_to `doctor`
- belongs_to `patient`
- has_one `recommendation` (trigger to close on recommendation created)

**Recommendation**
- text
- belongs_to `appointment`
- has_one `patient` through `appointments`
- has_one `doctor` through `appointments`

## Features:

**Authentication**:
devise phone+password

**Authorization**:
cancancan. User -> user's stuff, Doctor -> doctor's stuff, Admin -> all stuff.

**User**
- register (devise)
- profile
- show `doctors`, filter by`doctor_category`
- create `appointment`
- show `appointments` (cancel?)
- show `recommendations`

**Doctor**
- profile
- show `appointments` (patients?)
- give `recommendations` (close appointment)

**Admin**
- show all `patients`
- show + create `doctors`
- show + create `doctor_categories`
- assign `doctor` to `doctor_category`