# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Doctor leaves recommendations', type: :feature do
  let(:doctor) { create(:doctor, password: 'secret') }
  let(:patient) { create(:patient) }
  let(:description) { 'Hello, doctor. I\'m ill. What should I do?' }
  let(:response) { 'You should take a pill.' }

  before do
    create_list(:appointment, 5, doctor:)
    create(:appointment, doctor:, patient:, description:)
    create_list(:appointment, 3, doctor:, patient:)
  end

  scenario 'Doctor closes one of his appointments with a recommendation' do
    visit root_path

    expect(page).to have_text('Log in')

    fill_in 'Phone number', with: doctor.phone_number
    fill_in 'Password', with: 'secret'

    click_button 'Log in'

    signed_in_text = "Logged in as: #{doctor.full_name} (Doctor)"
    expect(page).to have_text(signed_in_text)

    appointment_card = find('.appointment.card') { |el| el.text.include?(description) }

    within appointment_card do
      fill_in(id: 'recommendation_content', with: response)
      click_button 'Give Recommendation'
    end

    expect(page).to have_current_path(doctor_home_path)
    expect(page).to have_text(description)
    expect(page).to have_text(response)
  end
end
