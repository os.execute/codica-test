# frozen_string_literal: true

class AppointmentsController < ApplicationController
  def close
    appointment = Appointment.find(params.dig(:recommendation, :appointment_id))

    authorize! :close, appointment
    Recommendation.create!(permitted_recommendation_params)
    redirect_back(fallback_location: root_path)
  end

  def new
    @doctor = Doctor.find(params[:doctor_id])
    @patient = current_user
    @appointment = Appointment.new(doctor: @doctor, patient: @patient)

    authorize! :create, @appointment
  end

  def create
    @doctor = Doctor.find(params[:doctor_id])
    appointment = Appointment.new(permitted_appointment_params)

    authorize! :create, appointment

    appointment.save!
    redirect_to patient_home_path
  end

  private

  def permitted_appointment_params
    return {} unless params.key?(:appointment)

    params[:appointment].permit(:description, :doctor_id, :patient_id)
                        .with_defaults(doctor_id: @doctor.id, patient_id: current_user.id)
  end

  def permitted_recommendation_params
    return {} unless params.key?(:recommendation)

    params[:recommendation].permit(:content, :appointment_id)
  end
end
