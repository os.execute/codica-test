# frozen_string_literal: true

ActiveAdmin.register Recommendation do
  menu priority: 8

  permit_params :appointment_id, :content

  filter :doctor
  filter :patient
  filter :content

  index do
    selectable_column
    id_column
    column :doctor
    column :patient
    column :appointment
    column :content
    actions
  end
end
